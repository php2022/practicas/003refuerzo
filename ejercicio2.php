<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        /**
         * OPCION 1
         */
        
        //crear un array directamente
        $vocales=[
            "a","e","i","o","u"
        ];
        
        var_dump($vocales);
        
        /**
         * OPCION 2
         */
        
        //crear un array con la funcion array
        $vocales=array(
            "a","e","i","o","u"
        );
        
        var_dump($vocales);
        
        ?>
    </body>
</html>
