<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $datos=[
            [
                "nombre"=>"Eva",
                "edad"=>50
            ],
            [
                "nombre"=>"Jose",
                "edad"=>40,
                "peso"=>80
            ]
        ];
        
        //introducir a "Lorena" de 80 años y con una altura de 175
        //Realizarlo directamente
        $datos=[
            "nombre"=>"Lorena",
            "edad"=>80,
            "altura"=>175
        ];
        var_dump($datos);
        
        //introducir a "Luis" de 20 años y con un peso de 90 y a "Oscar" de 23 años
        //Realizarlo mediante la funcion push
        array_push($datos,[
            "nombre"=>"Luis",
            "edad"=>20,
            "peso"=>90
            ],
            [
            "nombre"=>"Oscar",
            "edad"=>23
            ]);
        var_dump($datos);
        
        ?>
    </body>
</html>
