<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $datos=[
            [
                "nombre"=>"Eva",
                "edad"=>50
            ],
            [
                "nombre"=>"Jose",
                "edad"=>40,
                "peso"=>80
            ]
        ];
        
        //mostrar el numero de registros introducidos en datos(2)
        echo count($datos);
        
        //mostrar el numero de elementos que tiene el registro de Eva(2)
        echo count($datos[0]);
        
        ?>
    </body>
</html>
