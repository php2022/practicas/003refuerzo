<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $vocales=[
            "a","e","i","o","u"
        ];
        
        //mostrar el numero de elementos de vocales utilizando una funcion
        echo count($vocales);
        
        //mostrar una vocal aleatoria utilizando mt_rand
        $vocal=mt_rand(0,count($vocales)-1);
        echo $vocales[$vocal];
        
        //mostrar otra vocal aleatoria utilizando mt_rand
         $vocal1=mt_rand(0,count($vocales)-1);
        echo $vocales[$vocal1];
        
        ?>
    </body>
</html>
